//
// Created by sergey on 20.01.2021.
//

#include "image.h"
#include <inttypes.h>
#include <stdlib.h>


void swap_image_sizes(struct image* image){
    uint32_t a = image->height;
    image->height=image->width;
    image->width=a;

}

struct image image_init (uint64_t width,uint64_t height){
    struct pixel* pixel_array = malloc( sizeof(struct pixel) * width * height) ;
    struct image image = { width,height,pixel_array };
    return image;
}

void free_pixel_array(struct pixel* array){
    free(array);
}

void free_image(struct image* image) {
    free_pixel_array(image->data);
}


static struct pixel getPixel(struct image* const image,size_t x,size_t y){
    return (image->data[x*image->width+y]);
}

struct pixel* rotateImage(struct image* const source,enum direction direction){

    struct pixel* rotated_array = malloc (sizeof(struct pixel) * source->width * source->height);
    switch (direction) {
        case CLOCKWISE:
            for (size_t i=0;i<source->width;i++){
                for (size_t j=0;j<source->height;j++){
                    rotated_array[i*source->height+j] = getPixel(source,j,source->width-i-1);
                }
            }
            break;
        case COUNTERCLOCKWISE:
            for (size_t i=0;i<source->width;i++) {
                for (size_t j = 0; j < source->height; j++) {
                    rotated_array[i * source->height + j] = getPixel(source, source->height - j - 1, i);
                }
            }
            break;
        default:
            break;
    }



    return rotated_array;
}

